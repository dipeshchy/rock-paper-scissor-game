import React, { Component } from "react";

const PlayerCard = ({ color, symbol }) => {
  const style = {
    backgroundColor: color,
    backgroundImage: "url(./img/" + symbol + ".png)",
    borderRadius: 100,
    border: "2px solid white"
  };
  return <div style={style} className="player-card"></div>;
};

class Game extends Component {
  constructor(props) {
    super(props);
    this.symbols = ["paper", "rock", "scissor"];
    this.state = {};
  }

  decideWinner = () => {
    const { playerRed, playerBlue } = this.state;
    if (playerRed === playerBlue) {
      return "It's a Draw";
    }
    if (
      (playerRed === "rock" && playerBlue === "scissor") ||
      (playerRed === "paper" && playerBlue === "rock") ||
      (playerRed === "scissor" && playerBlue === "paper")
    ) {
      return "Winner is Player Red";
    } else {
      return "Winner is Player Blue";
    }
  };

  runGame = () => {
    let counter = 0;
    let myIntervel = setInterval(() => {
      counter++;
      this.setState({
        playerRed: this.symbols[Math.floor(Math.random() * 3)],
        playerBlue: this.symbols[Math.floor(Math.random() * 3)],
        winner: ""
      });
      if (counter > 20) {
        clearInterval(myIntervel);
        this.setState({ winner: this.decideWinner() });
      }
    }, 100);
  };

  render() {
    return (
      <div className="container main-board">
        <h1 className="text-center mt-3 pt-2 text-danger">
          Rock Paper Scissor Game
        </h1>
        <div className="text-center">
          <PlayerCard color="red" symbol={this.state.playerRed} />
          <PlayerCard color="blue" symbol={this.state.playerBlue} />
          <div className="text-center mt-2">
            <h3 className="mb-3">{this.state.winner}</h3>
            <button className="btn btn-danger btn-lg" onClick={this.runGame}>
              Run Game
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default Game;
